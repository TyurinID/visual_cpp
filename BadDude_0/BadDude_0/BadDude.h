#pragma once
#include "Gunslinger.h"
#include "PokerPlayer.h"

class BadDude :
	public Gunslinger, PokerPlayer
{
public:
	BadDude();
	~BadDude();

	int Pdraw();
	double Gdraw();
	void Show();
};



BadDude::BadDude() {
}


BadDude::~BadDude() {
}

inline int BadDude::Pdraw() {
	return PokerPlayer::Draw();
}

inline double BadDude::Gdraw() {
	return Gunslinger::Draw();
}

inline void BadDude::Show() {
	Gunslinger::Show();
	cout << "Card: " << Pdraw() << endl;
	cout << endl;
}
