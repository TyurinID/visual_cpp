#pragma once
#include "stdafx.h"
#include "Person.h"

class Gunslinger :
	 public virtual Person
{
public:
	Gunslinger();
	Gunslinger(int barrels);
	~Gunslinger();

	double Draw();
	void Show();
private:
	int RifleBarrel;
};



Gunslinger::Gunslinger() {
}

inline Gunslinger::Gunslinger(int barrels) {
	this->RifleBarrel = barrels;
}


Gunslinger::~Gunslinger() {
}

inline double Gunslinger::Draw() {
	return rand() % 100;
}

inline void Gunslinger::Show() {
	Person::Show();
	cout << "Exit Time: " << to_string(Draw()) << endl;
	cout << "Rifle Barrels: " << RifleBarrel << endl;
	cout << endl;	
}
