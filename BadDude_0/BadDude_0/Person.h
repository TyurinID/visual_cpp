#pragma once
#include "stdafx.h"

class Person
{
public:
	Person();
	~Person();

	virtual void Show();
	void SetData(string name, string surname);

private:
	string name;
	string surname;
};



Person::Person() :name("No_name"), surname("No_surname") {
}


Person::~Person() {
}

inline void Person::Show() {
	cout << "Name: " << this->name << ". Surname: " << this->surname << endl;
	cout << endl;
}

inline void Person::SetData(string name, string surname) {
	this->name		= name;
	this->surname	= surname;
}
