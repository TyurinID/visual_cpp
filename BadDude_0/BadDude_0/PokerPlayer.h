#pragma once
#include "stdafx.h"
#include "Person.h"

class PokerPlayer :
	 public virtual Person
{
public:
	PokerPlayer();
	~PokerPlayer();

	double Draw();
	virtual void Show();
};



PokerPlayer::PokerPlayer() {
}


PokerPlayer::~PokerPlayer() {
}

inline double PokerPlayer::Draw() {	
	return rand() % 52;
}

inline void PokerPlayer::Show() {
	Person::Show();
	cout << "Card: " << to_string(Draw()) << endl;
	cout << endl;
}
