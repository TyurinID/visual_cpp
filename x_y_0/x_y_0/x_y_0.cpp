// x_y_0.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{
	int x = 25;
	int y = 10;
	cout << "x = " << x << " y = " << y << endl;
	x = x + y;
	y = x - y; 
	x = x - y;
	cout << "x = " << x << " y = " << y << endl;

    return 0;
}

